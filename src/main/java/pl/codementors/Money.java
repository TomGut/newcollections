package pl.codementors;

public class Money implements Comparable<Money>{
    private int value;

    public Money(int value){
        this.value = value;
    }

    @Override
    public String toString() {
        return "Money{" + "value=" + value + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        return value == money.value;
    }

    @Override
    public int hashCode() {
        return value;
    }

    public static void main(String[] args) {
        Money a = new Money(100);
        Money b = new Money(100);

        System.out.println(a==b);
        //tu equals daje false bo dziedziczy z Object czyli porównuje referencje obiektów stąd false, robi to co wyżej
        //teraz musimy zaimplementować equals i hascode z alt+ins - linia 15 i 25 i teraz daje true
        //equals i has daje odp typu boolena - czy jestes taki sam ?
        System.out.println(a.equals(b));
    }

    @Override
    public int compareTo(Money o) {
        return this.value - o.value;
    }
}
