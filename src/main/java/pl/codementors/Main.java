package pl.codementors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        /*
        alt+ctrl+B - poda listę klas które implementuje interfejs List
        po lewej znaku = musi być interfejs a po prawej typ listy
        jeżeli chcemy poruszać się po indeksie to lepiej po tablicy czyli po arraylist
        jeżeli iterować to linklist

        ta konkretna lista jest w generyku i przyjmuje tylko stringi - typujemy kolekcje
        generyki są dostępne tylko na poziomie kompilacji
        */
        List<String> list = new ArrayList<>();
        //można też nie uzywać generyków - ale przy wyciąganiu musimy rzutować
        List list2 = new ArrayList();

        //dodajemy pojedyncze elementy do list
        list.add("Kasia");
        list.add("Janek");

        List<String> names = new ArrayList<>();
        names.add("Wojtek");
        names.add("Tomek");
        names.add("Kasia");
        //można to samo osiągnąć szybciej
        List<String> names2 = Arrays.asList("Wojtek", "Tomek", "Kasia");
        System.out.println("To jest names2");
        System.out.println(names2);

        //do list dodajemy całą kolekcję names
        list.addAll(names);

        System.out.println("To jest list");
        System.out.println(list);

        //usuwamy pojedyncze elementy z list
        list.remove("Kasia");;
        System.out.println("To jest list po usunięciu Kasia");
        System.out.println(list);

        //sortowanie porządek naturalny - tu importujemy klasę Collections (ma dodatkowe metody) a nie Collection
        System.out.println("Sortuje w poż naturalnym");
        Collections.sort(list);
        //lub z kilkoma parametrami
        System.out.println("Sortuje od tyłu");
        Collections.sort(list, Collections.reverseOrder());
        System.out.println("To jest list po sortowaniu");
        System.out.println(list);

        //interfejs comparable sortuje naturalnym pożądkiem - porównuje jakiś przysłany obiejk z tym co implementuje interfejs
        //Comparator - może być ich wiele i może sortować różne algorytmy - porównuje dwa obiekty
    }
}
